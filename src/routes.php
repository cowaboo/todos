<?php
require_once '../Models/TodoList.php';
require_once '../Models/User.php';
// Routes

// $app->get('/[{name}]', function ($request, $response, $args) {
//     // Sample log message
//     $this->logger->info("Slim-Skeleton '/' route");

//     // Render index view
//     return $this->renderer->render($response, 'index.phtml', $args);
// });

/**
 * @return mixed
 */
function getCurrentSecretKey()
{
    return $_COOKIE['secretKey'];
    return 'SDRGPHVQWQFTQP3CQH4ZT6XRNAU6AIQ7OHZAMBT2SPCUHWHSDOBWKHFI';
}

/**
 * @param $secretKey
 */
function saveSecretKey($secretKey = '')
{
    setcookie('secretKey', $secretKey, 0, '/');
}

function forgetSecretKey()
{
    setcookie('secretKey', '', time() - 3600, '/');
    unset($_COOKIE['secretKey']);
}

$todoLists             = false;
$unsubscribedTodoLists = false;
$secretKey             = false;
if (getCurrentSecretKey()) {
    $info                  = User::getInfo(getCurrentSecretKey());
    $todoLists             = $info->observatories->subscribed;
    $unsubscribedTodoLists = $info->observatories->unsubscribed;
    $secretKey             = getCurrentSecretKey();
    $email                 = $info->email;
}

// $twig = $app->view->getEnvironment();
// var_dump($app);
// var_dump($app->getContainer()['view']);
// var_dump($twig);
// die();
$twig = $app->getContainer()['view'];

// $twig->addGlobal('todoLists', $todoLists);
// $twig->addGlobal('unsubscribedTodoLists', $unsubscribedTodoLists);
// $twig->addGlobal('secretKey', $secretKey);
$twig['todoLists']             = $todoLists;
$twig['unsubscribedTodoLists'] = $unsubscribedTodoLists;
$twig['secretKey']             = $secretKey;
$twig['email']                 = $email;

// Render Twig template in route
$app->get('/', function ($request, $response, $args) {
    $currentRoute = 'home';
    return $this->view->render($response, 'views/home.html.twig', compact('currentRoute'));
})->setName('home');

$app->get('/list/new', function ($request, $response, $args) {
    $currentRoute = 'list.new';
    return $this->view->render($response, 'views/list.new.html.twig', compact('currentRoute'));
})->setName('list.new');

$app->get('/list/{todolist}', function ($request, $response, $args) use ($todoLists, $email) {

    $todolistId   = $request->getAttribute('todolist');
    $subscribed   = isset($todoLists[$todolistId]);
    $currentRoute = 'list.show.'.$todolistId;
    $todolist     = TodoList::get($todolistId);
    $member_list  = $todolist['member_list'];
    $admin        = $todolist['admin'];
    $todolist     = $todolist['list'];
    $is_admin     = in_array($email, $admin);

    return $this->view->render($response, 'views/list.show.html.twig', compact('currentRoute', 'todolistId', 'todolist', 'subscribed', 'admin', 'is_admin', 'member_list'));
})->setName('list.show');

$app->post('/secretKey', function ($request, $response, $args) use ($app) {
    $vars      = $request->getParsedBody();
    $secretKey = $vars['secretKey'];
    if (!isset($vars['secretKey']) || !$vars['secretKey']) {
        throw new Exception("Aucune clé entrée !");
    }

    saveSecretKey($secretKey);

    return $response->withStatus(302)->withHeader('Location', $app->getContainer()->get('router')->pathFor('home'));
})->setName('secretKey.save');

$app->post('/secretKey/forget', function ($request, $response, $args) use ($app) {
    forgetSecretKey();
    return $response->withStatus(302)->withHeader('Location', $app->getContainer()->get('router')->pathFor('home'));
})->setName('secretKey.forget');

$app->post('/list/create', function ($request, $response, $args) use ($app) {
    $vars      = $request->getParsedBody();
    $secretKey = getCurrentSecretKey();
    if (!isset($vars['listName']) || !$vars['listName']) {
        throw new Exception("Création de liste sans nom de liste !");
    }
    $todolistId = $vars['listName'];
    TodoList::create($secretKey, $todolistId);

    return $response->withStatus(302)->withHeader('Location', $app->getContainer()->get('router')->pathFor('list.show', [
        'todolist' => $todolistId
    ]));
})->setName('list.create');

$app->post('/list/{todolist}/delete', function ($request, $response, $args) use ($app) {
    $vars       = $request->getParsedBody();
    $secretKey  = getCurrentSecretKey();
    $todolistId = $request->getAttribute('todolist');
    if (!$todolistId) {
        throw new Exception("Suppression de liste sans nom de liste !");
    }
    TodoList::delete($secretKey, $todolistId);

    return $response->withStatus(302)->withHeader('Location', $app->getContainer()->get('router')->pathFor('home'));
})->setName('list.delete');

$app->post('/list/{todolist}/new', function ($request, $response, $args) use ($app) {
    $todolistId = $request->getAttribute('todolist');
    $vars       = $request->getParsedBody();
    $secretKey  = getCurrentSecretKey();
    if (!$todolistId) {
        throw new Exception("Création de tâche sans liste !");
    }
    if (!isset($vars['taskName']) || !$vars['taskName']) {
        throw new Exception("Création de tâche sans intitulé !");
    }
    $taskName = $vars['taskName'];
    $addTask  = TodoList::addTask($secretKey, $todolistId, $taskName);

    return $response->withStatus(302)->withHeader('Location', $app->getContainer()->get('router')->pathFor('list.show', [
        'todolist' => $todolistId
    ]));
})->setName('list.create.task');

$app->post('/list/{todolist}/suscribe', function ($request, $response, $args) use ($app) {
    $todolistId = $request->getAttribute('todolist');
    $secretKey  = getCurrentSecretKey();
    TodoList::suscribe($secretKey, $todolistId);

    return $response->withStatus(302)->withHeader('Location', $app->getContainer()->get('router')->pathFor('list.show', [
        'todolist' => $todolistId
    ]));
})->setName('list.user.suscribe');

$app->post('/list/{todolist}/unsuscribe', function ($request, $response, $args) use ($app) {
    $todolistId = $request->getAttribute('todolist');
    $secretKey  = getCurrentSecretKey();
    TodoList::unsuscribe($secretKey, $todolistId);

    return $response->withStatus(302)->withHeader('Location', $app->getContainer()->get('router')->pathFor('list.show', [
        'todolist' => $todolistId
    ]));
})->setName('list.user.unsuscribe');

$app->post('/list/{todolist}/unadmin', function ($request, $response, $args) use ($app) {
    $todolistId = $request->getAttribute('todolist');
    $secretKey  = getCurrentSecretKey();
    TodoList::unadmin($secretKey, $todolistId);

    return $response->withStatus(302)->withHeader('Location', $app->getContainer()->get('router')->pathFor('list.show', [
        'todolist' => $todolistId
    ]));
})->setName('list.user.unadmin');

$app->post('/list/{todolist}/admin', function ($request, $response, $args) use ($app) {
    $todolistId = $request->getAttribute('todolist');
    $secretKey  = getCurrentSecretKey();
    $vars       = $request->getParsedBody();
    if (!isset($vars['email']) || !$vars['email']) {
        throw new Exception("Ajout d'un admin sans email !");
    }
    $email = $vars['email'];
    TodoList::admin($secretKey, $todolistId, $email);

    return $response->withStatus(302)->withHeader('Location', $app->getContainer()->get('router')->pathFor('list.show', [
        'todolist' => $todolistId
    ]));
})->setName('list.user.admin');

$app->post('/task/modify', function ($request, $response, $args) use ($app) {
    $vars      = $request->getParsedBody();
    $secretKey = getCurrentSecretKey();
    if (!isset($vars['todolistId']) || !$vars['todolistId']) {
        throw new Exception("Modification de tâche sans liste !");
    }
    if (!isset($vars['hash']) || !$vars['hash']) {
        throw new Exception("Modification de tâche sans hash !");
    }
    if (!isset($vars['newValue']) || !$vars['newValue']) {
        throw new Exception("Modification de tâche sans nouvelle valeur !");
    }
    $modifyTask = TodoList::modifyTask($secretKey, $vars['todolistId'], $vars['hash'], $vars['newValue']);

    return $response->withStatus(302)->withHeader('Location', $app->getContainer()->get('router')->pathFor('list.show', [
        'todolist' => $vars['todolistId']
    ]));
})->setName('task.modify');

$app->post('/task/delete', function ($request, $response, $args) use ($app) {
    $vars      = $request->getParsedBody();
    $secretKey = getCurrentSecretKey();
    if (!isset($vars['todolistId']) || !$vars['todolistId']) {
        throw new Exception("Suppression de tâche sans liste !");
    }
    if (!isset($vars['hash']) || !$vars['hash']) {
        throw new Exception("Suppression de tâche sans hash !");
    }
    $modifyTask = TodoList::deleteTask($secretKey, $vars['todolistId'], $vars['hash']);

    return $response->withStatus(302)->withHeader('Location', $app->getContainer()->get('router')->pathFor('list.show', [
        'todolist' => $vars['todolistId']
    ]));
})->setName('task.delete');
