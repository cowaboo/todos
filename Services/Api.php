<?php

class Api
{
    /**
     * @var string
     */
    public $domain = 'http://sandbox.cowaboo.net/api/';

    /**
     * @param $listName
     */
    public static function get($url, $args = array())
    {
        $api = new self();
        $url = $api->domain.$url.'?'.http_build_query($args);

        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);
        return json_decode($output);

        // close curl resource to free up system resources
        curl_close($ch);
    }

    /**
     * @param $listName
     */
    public static function post($url, $args = array(), $method = 'POST')
    {
        // var_dump($args);
        // die();
        $api = new self();
        $url = $api->domain.$url;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);

        // in real life you should use something like:
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($args));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);
        // echo ($server_output);
        // die();
        curl_close($ch);

        return true;
    }
}
