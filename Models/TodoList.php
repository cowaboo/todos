<?php
require_once '../Services/Api.php';
require_once 'User.php';

class TodoList {
	/**
	 * @param $listName
	 */
	public static function addTask($secretKey, $todolistId, $taskName) {
		$observatoryId = $todolistId;
		$tags = $taskName;
		$value = 'pending';
		return Api::post('entry', compact('secretKey', 'observatoryId', 'tags', 'value'));
	}

	/**
	 * @param $secretKey
	 * @param $todolistId
	 */
	public static function admin($secretKey, $todolistId, $email) {
		$todolist = self::get($todolistId);
		$admin = $todolist['admin'];

		if ($admin && (array_search($email, $admin) !== false)) {
			return true;
		}

		$admin = $admin ? $admin : array();
		$admin[] = $email;
		$observatoryId = $todolistId;
		$metadata_name = 'todo-admin';
		// $metadata_value = json_encode($admin);
		$metadata_value = json_encode($admin);
		return Api::post('observatory/conf', compact('secretKey', 'observatoryId', 'metadata_name', 'metadata_value'));
	}

	/**
	 * @param $listName
	 */
	public static function create($secretKey, $todolistId) {
		$observatoryId = $todolistId;
		$full = true;
		return Api::post('observatory', compact('secretKey', 'observatoryId', 'full'));
	}

	/**
	 * @param $listName
	 */
	public static function delete($secretKey, $todolistId) {
		$observatoryId = $todolistId;
		$full = true;
		return Api::post('observatory', compact('secretKey', 'observatoryId'), 'DELETE');
	}

	/**
	 * @param $secretKey
	 * @param $todolistId
	 * @param $hash
	 */
	public static function deleteTask($secretKey, $todolistId, $hash) {
		$observatoryId = $todolistId;
		return Api::post('entry', compact('secretKey', 'observatoryId', 'hash'), 'DELETE');
	}

	/**
	 * @return mixed
	 */
	public static function get($todolistId) {
		$observatoryId = $todolistId;
		$todoList = Api::get('observatory', compact('observatoryId'))->dictionary;
		$member_list = isset($todoList->member_list) ? $todoList->member_list : array();
		$conf = isset($todoList->conf) ? $todoList->conf : array();
		$id = 'todo-admin';
		$admin = isset($conf->$id) ? $conf->$id : array();

		$list = array();
		foreach ($todoList->entries as $hash => $entry) {
			$entry->label = str_replace('|', '', $entry->tags);
			$entry->hash = $hash;
			$list[] = $entry;
			usort($list, function ($a, $b) {
				if ('done' == $a->value) {
					return ($a->value == $b->value) ? strcasecmp($a->label, $b->label) : 1;
				}
				return $a->value == $b->value ? strcasecmp($a->label, $b->label) : -1;
			});
		}
		return array(
			'admin' => $admin,
			'member_list' => $member_list,
			'list' => $list,
		);
	}

	/**
	 * @return mixed
	 */
	public static function getAll() {
		$todoLists = Api::get('tags');
		$list = array();
		foreach ($todoLists->tag_list->list as $todo => $tags) {
			$list[$todo] = $tags;
		}
		return $list;
	}

	/**
	 * @param $secretKey
	 * @param $todolistId
	 * @param $hash
	 * @param $newValue
	 */
	public static function modifyTask($secretKey, $todolistId, $hash, $newValue) {
		$observatoryId = $todolistId;
		return Api::post('entry', compact('secretKey', 'observatoryId', 'hash', 'newValue'), 'PUT');
	}

	/**
	 * @param $listName
	 */
	public static function suscribe($secretKey, $todolistId) {
		$observatoryId = $todolistId;
		return Api::post('user/observatories', compact('secretKey', 'observatoryId'));
	}

	/**
	 * @param $secretKey
	 * @param $todolistId
	 */
	public static function unadmin($secretKey, $todolistId) {
		$user = User::getInfo($secretKey);
		$email = $user->email;
		$todolist = self::get($todolistId);
		$admin = $todolist['admin'];

		if (($key = array_search($email, $admin)) !== false) {
			unset($admin[$key]);
			$admin = array_merge([], $admin);

		}

		$observatoryId = $todolistId;
		$metadata_name = 'todo-admin';

		$metadata_value = json_encode($admin);
		if (!$admin) {
			$admin = '';
			$metadata_value = '';
		}
		// $metadata_value = $admin;
		// var_dump($metadata_value);
		// die();

		return Api::post('observatory/conf', compact('secretKey', 'observatoryId', 'metadata_name', 'metadata_value'));
	}

	/**
	 * @param $listName
	 */
	public static function unsuscribe($secretKey, $todolistId) {
		$observatoryId = $todolistId;
		return Api::post('user/observatories', compact('secretKey', 'observatoryId'), 'DELETE');
	}
}
