<?php
require_once '../Services/Api.php';

class User
{
    /**
     * @return mixed
     */
    public static function getInfo($secretKey)
    {
        $info = Api::get('user', compact('secretKey'));

        $list = array();
        foreach ($info->observatories->subscribed as $todo => $tags) {
            $list[$todo] = $tags;
        }
        $info->observatories->subscribed = $list;

        $list = array();
        foreach ($info->observatories->unsubscribed as $todo => $tags) {
            $list[$todo] = $tags;
        }
        $info->observatories->unsubscribed = $list;

        return $info;
    }
}
